package com.pointr.challenge

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class Question2Activity : AppCompatActivity() {

    /***/
    private val ENGLISH_LOW_CASE_ALPHABET_CHAR_CODES = 97..122

    /***/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question_2)

        val actionButton = findViewById<Button>(R.id.action_button)
        val resultTextView = findViewById<TextView>(R.id.result)
        val inputField = findViewById<TextView>(R.id.input)

        actionButton.setOnClickListener { _ ->

            val inputText = inputField.text.toString()

            if (inputText.isEmpty()) {
                Toast.makeText(this, "Field is empty", Toast.LENGTH_SHORT).show()
                resultTextView.text = ""
                return@setOnClickListener
            }

            if (containsNotEnglishLowCaseAlphabetSymbols(inputText)) {
                Toast.makeText(this, "String contains not english alphabet low case symbols", Toast.LENGTH_SHORT).show()
                resultTextView.text = ""
                return@setOnClickListener
            }

            resultTextView.text = if (isAnagramOfPalindrome(inputText)) {
                "Yes"
            } else {
                "No"
            }
        }

    }

    /***/
    private fun isAnagramOfPalindrome(string: String): Boolean {

        /**
         * Create map of <Char code, Counter of that char code>
         */
        val map: MutableMap<Int, Int> = mutableMapOf()
        ENGLISH_LOW_CASE_ALPHABET_CHAR_CODES.forEach { map.put(it, 0) }

        /**
         * Count all the char codes in the given string
         * This is the ony loop which depends on N,
         * so we have complexity O(N) here
         */
        for (c in string.toCharArray()) {
            /**
             * Nested loop, but with the constant repeat time (26 times)
             */
            ENGLISH_LOW_CASE_ALPHABET_CHAR_CODES.forEach {
                if (c.toInt() == it) {
                    map.put(it, map[it]!! + 1)
                }

            }
        }

        /**
         * Logic is:
         * if we have more then one character which appears in string an odd number of times
         * then this is NOT anagram of palindrome. Because palindrome is a symmetrical string.
         * That mean it should be:
         * - each character should appears an even number of times
         * OR
         * - each character should appears an even number of times AND one symbol appears ONE time
         */

        var oddNumbersCounter = 0

        map.forEach { entry ->
            if (entry.value % 2 == 1) {
                oddNumbersCounter++
            }
        }

        return oddNumbersCounter <= 1

    }

    private fun containsNotEnglishLowCaseAlphabetSymbols(string: String): Boolean {
        /**
         * Here we have another loop which depends on N,
         * but that loop is just for UI validation, not for the main method
         */
        return string.toCharArray()
                .filterNot { c -> c.toInt() in ENGLISH_LOW_CASE_ALPHABET_CHAR_CODES }
                .isNotEmpty()
    }

}