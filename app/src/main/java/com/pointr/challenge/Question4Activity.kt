package com.pointr.challenge

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.webkit.WebView
import android.widget.TextView

class Question4Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question_4)

        val textView1 = findViewById<WebView>(R.id.text_1)

        //textView1.text = Html.fromHtml(getString(R.string.question_4_answer))
        textView1.isVerticalScrollBarEnabled = false
        textView1.isHorizontalScrollBarEnabled = false
        textView1.loadData(getString(R.string.question_4_answer), "text/html","utf-8")

    }

}