package com.pointr.challenge

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView
import com.pointr.challenge.Question1Activity.Companion.a
import com.pointr.challenge.Question1Activity.Companion.c
import com.pointr.challenge.Question1Activity.Companion.m
import com.pointr.challenge.Question1Activity.Companion.max

class Question1Activity : AppCompatActivity() {

    /***/
    private var last = (System.currentTimeMillis() % max).toInt()

    /***/
    companion object {
        val m = 32768
        val a = 25173
        val c = 13849
        val max = 6
        val testCount = 10000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question_1)

        val actionButton = findViewById<Button>(R.id.action_button)
        val testButton = findViewById<Button>(R.id.test_button)
        val resultTextView = findViewById<TextView>(R.id.result_text_view)
        val explanationTextView = findViewById<TextView>(R.id.explanation_text_view)

        actionButton.setOnClickListener { _ ->
            explanationTextView.text = "Current dice value"
            resultTextView.text = rollDice().toString()

        }

        testButton.setOnClickListener { _ ->
            explanationTextView.text = "Mean deviation for $testCount tries"
            resultTextView.text = String.format("%.2f%%", testRollDice() * 100)
        }

    }


    /***/
    private fun rollDice(): Int {
        last = (last * a + c) % m
        return last % max + 1
    }

    private fun testRollDice(): Float {

        val map: MutableMap<Int, Int> = mutableMapOf()
        for (i in 1..max) {
            map.put(i, 0)
        }

        var currentValue: Int
        for (i in 0..testCount) {
            currentValue = rollDice()
            map.put(currentValue, map[currentValue]!! + 1)
        }

        val perfectDistribution = 1F / max.toFloat()

        return map
                .map { entry -> Math.abs(perfectDistribution - entry.value.toFloat() / testCount.toFloat()) }
                .max()!!

    }

}