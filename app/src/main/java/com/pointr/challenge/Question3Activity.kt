package com.pointr.challenge

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.widget.Button
import android.widget.LinearLayout

class Question3Activity : AppCompatActivity() {

    private var pressedTime = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question_3)
/*
        findViewById<Button>(R.id.action_button).setOnClickListener{view ->
            // check press

        }
*/


        findViewById<Button>(R.id.action_button).setOnTouchListener{view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    pressedTime = System.currentTimeMillis()
                    return@setOnTouchListener true
                }
                MotionEvent.ACTION_UP -> {
                    Log.i("test", (System.currentTimeMillis() - pressedTime).toString())
                    when (System.currentTimeMillis() - pressedTime){
                        in (0 until 1000) -> changeScreenColor(android.R.color.white)
                        in (1000 until 2000) -> changeScreenColor(R.color.blue_100)
                        in (2000 until 3000) -> changeScreenColor(R.color.green_100)
                        in (3000 until 4000) -> changeScreenColor(R.color.yellow_100)
                        in (4000 until 5000) -> changeScreenColor(R.color.orange_100)
                        else -> changeScreenColor(R.color.red_100)
                    }
                    pressedTime = 0L
                    return@setOnTouchListener false
                }
                else -> return@setOnTouchListener false
            }
        }
    }

    /***/
    private fun changeScreenColor(colorResId:  Int){
           findViewById<LinearLayout>(R.id.root).setBackgroundColor(
                   ContextCompat.getColor(this, colorResId)
           )
    }

}