package com.pointr.challenge

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.question_1_button).setOnClickListener {
            startActivity(Intent(this, Question1Activity::class.java))
        }
        findViewById<Button>(R.id.question_2_button).setOnClickListener {
            startActivity(Intent(this, Question2Activity::class.java))
        }
        findViewById<Button>(R.id.question_3_button).setOnClickListener {
            startActivity(Intent(this, Question3Activity::class.java))
        }
        findViewById<Button>(R.id.question_4_button).setOnClickListener {
            startActivity(Intent(this, Question4Activity::class.java))
        }

    }
}
